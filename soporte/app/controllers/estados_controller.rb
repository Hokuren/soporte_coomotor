class EstadosController < ApplicationController
  before_action :get_Estado,only: [:update,:show,:destroy,:edit]
  def index
    @estados = Estado.all
  end

  def new
    @estado = Estado.new
  end
  def edit
  end

  def create
    @estado = Estado.new(estados_params)
    respond_to do |format|
      if @estado.save
        format.html { redirect_to @estado, notice: 'el estado fue creado con exito' }
        format.json { render :show, status: :created, location: @estado }
      end
    end
  end

  def show
  end
  def update
    respond_to do |format|
      if @estado.update(estados_params)
        format.html{redirect_to @estado,notice: 'estado editado'}
        format.json{redirect_to @estado}
      end
    end
  end

  def destroy
    @estado.destroy
    respond_to do |format|
      format.html { redirect_to estados_path, notice: 'Estado Eliminado' }
      format.json { head :no_content }
    end
  end

  private
  def get_Estado
    @estado = Estado.find(params[:id])
  end
  def estados_params
    params.require(:estado).permit(:id,:descripcion)
  end
end
